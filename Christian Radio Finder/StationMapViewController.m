//
//  StationMapViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 8/30/12.
//
//

#import "StationMapViewController.h"

@interface StationMapViewController ()

@end

@implementation StationMapViewController

@synthesize map;
@synthesize locationManager;
@synthesize stationArray;
@synthesize biggestLat;
@synthesize biggestLng;
@synthesize smallestLat;
@synthesize smallestLng;
@synthesize isSearching;
@synthesize searchLocation;
@synthesize infoLabel;
@synthesize helpText;
@synthesize locationHitCount;
@synthesize selectedCircle;

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [geocoder geocodeAddressString:searchBar.text completionHandler:^(NSArray *placemarks, NSError *error) {
        //Error checking
        
        if (placemarks.count == 0) {
            [self searchStations:searchBar.text];
            return;
        }
        
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        [self searchAtLocation:placemark.region.center.latitude :placemark.region.center.longitude: NO];
        
//        MKCoordinateRegion region;
//        region.center.latitude = placemark.region.center.latitude;
//        region.center.longitude = placemark.region.center.longitude;
//        MKCoordinateSpan span;
//        double radius = placemark.region.radius / 1000; // convert to km
//        
//        NSLog(@"Radius is %f", radius);
//        span.latitudeDelta = radius / 112.0;
//        
//        region.span = span;
//        
//        [map setRegion:region animated:YES];
    }];
}

- (void) searchStations: (NSString *) stationName {
    StationLookupManager *stationLookupManager = [[StationLookupManager alloc] init];
    
    [stationLookupManager getData: stationName :^(NSString *errorMessage) {
        NSLog(@"%@",errorMessage);
        UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorView show];
    } :^(NSMutableArray *xmlStationArray) {
        [self setStationArray:xmlStationArray];
        [self addOverlayFromStationArray];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) searchAtLocation: (double) lat : (double) lng : (BOOL) currentLocation{
    CLLocationCoordinate2D searchPoint = CLLocationCoordinate2DMake(lat, lng);
    
    [self setSearchLocation:searchPoint];
    
    [map setShowsUserLocation:currentLocation];
    
    //Remove all added annotations
    [map removeAnnotations:map.annotations];
    
    [map removeOverlays:map.overlays];
    
    if (currentLocation == NO) {
        MKPointAnnotation *pressPin = [[MKPointAnnotation alloc] init];
        [pressPin setCoordinate:searchPoint];
        
        [pressPin setTitle:@"Dropped Pin"];
        
        [map addAnnotation:pressPin];
    }
    
    
    StationLookupManager *stationLookupManager = [[StationLookupManager alloc] init];
    
    [stationLookupManager getData:searchPoint.latitude :searchPoint.longitude :0 :^(NSString *errorMessage) {
        NSLog(@"%@",errorMessage);
        UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorView show];
    } :^(NSMutableArray *xmlStationArray) {
        NSMutableArray *bindableArray = [[NSMutableArray alloc]init];
        for (int a = 0; a < [xmlStationArray count]; a++) {
            Station *newStation = (Station *)[xmlStationArray objectAtIndex:a];
            CLLocationDegrees lat = [newStation.lat doubleValue];
            CLLocationDegrees lng = [newStation.lng doubleValue];
            CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            CLLocationDistance distance = [[[CLLocation alloc] initWithLatitude:searchPoint.latitude longitude:searchPoint.longitude ] distanceFromLocation:radioLocation] * 0.000621371192;
            [newStation setDistance:[NSString stringWithFormat:@"%d miles", (int) ceil(distance)]];
            if (distance <= [newStation.range doubleValue]) {
                //[xmlStationArray replaceObjectAtIndex:a withObject:newStation];
                [bindableArray addObject:newStation];
            }
            
        }
        
        
        [self setStationArray:bindableArray];
        [self addOverlayFromStationArray];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

- (IBAction)longPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan){
        return;
    }

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    isSearching = YES;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:map];
    
    CLLocationCoordinate2D touchLatLng = [map convertPoint:touchPoint toCoordinateFromView:map];
    //[self setSearchLocation:touchLatLng];
    
    NSLog(@"got long press at %lf, %lf",touchLatLng.latitude,touchLatLng.longitude);
    
    if ([helpText isEqualToString:@"Press and hold to search"]) {
        [self setHelpText:@"Use action menu to use GPS"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:helpText forKey:@"mapHelpText"];
        [defaults synchronize];
        [infoLabel setText:helpText];
    }
    
    [self searchAtLocation:touchLatLng.latitude :touchLatLng.longitude : NO];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    if( annotation == map.userLocation ){ return nil; }

    
    MKPinAnnotationView *customPinview = [[MKPinAnnotationView alloc]
                                           initWithAnnotation:annotation reuseIdentifier:nil];
    CLLocationCoordinate2D point = [annotation coordinate];

    if (isSearching == YES) {
        customPinview.pinColor = MKPinAnnotationColorGreen;
    }
    else {
        customPinview.pinColor = MKPinAnnotationColorRed;
    }
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    customPinview.rightCalloutAccessoryView = rightButton;
    
    if (point.latitude == searchLocation.latitude && point.longitude == searchLocation.longitude) {
        customPinview.pinColor = MKPinAnnotationColorPurple;
        customPinview.rightCalloutAccessoryView = nil;
    }
    customPinview.animatesDrop = YES;
    customPinview.canShowCallout = YES;
    
    return customPinview;
}





- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    
    
    
    [self performSegueWithIdentifier:@"ShowDetailsFromMapSegue" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"ShowDetailsFromMapSegue"]) {
        NSString *title = [[[map selectedAnnotations] objectAtIndex:0]title];
        NSLog(@"call out %@",title);
        Station *selectedStation = nil;
        for (int a = 0; a < [stationArray count]; a++) {
            Station *s = (Station *)[stationArray objectAtIndex:a];
            if ([s.frequency isEqualToString:title]) {
                selectedStation = s;
                break;
            }
        }
        
        if (!selectedStation) {
            return;
        }
        
        NSLog(@"station info %@",selectedStation.ownedBy);
        
        StationDetailTableViewController *controller = [segue destinationViewController];
        [controller setStation:selectedStation];
        return;
        
    }
}


-(void) addOverlayFromStationArray{
    if (stationArray.count ==0) {
        return;
    }
    biggestLat = 0;
    biggestLng = 0;
    smallestLat = 0;
    smallestLng = 0;
    for (int a = 0; a < [stationArray count]; a++) {
        Station *station = (Station *)[stationArray objectAtIndex:a];
        CLLocationDegrees lat = [station.lat doubleValue];
        CLLocationDegrees lng = [station.lng doubleValue];
        CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
        double decimalRange = [station.range doubleValue] / 69;
        if ((lat + decimalRange) > biggestLat || biggestLat == 0) {
            biggestLat = lat + decimalRange;
        }
        if ((lat - decimalRange) < smallestLat || smallestLat == 0) {
            smallestLat = lat - decimalRange;
        }
        if ((lng + decimalRange) > biggestLng || biggestLng == 0) {
            biggestLng = lng + decimalRange;
        }
        if ((lng - decimalRange) < smallestLng || smallestLng == 0) {
            smallestLng = lng - decimalRange;
        }
        MKCircle *circle = [MKCircle circleWithCenterCoordinate:radioLocation.coordinate radius:[station.range doubleValue] * 1609];
        [circle setSubtitle:station.ownedBy];
        [circle setTitle:station.frequency];
        
        [map addOverlay:circle];
        
        MKPointAnnotation *stationLocation = [[MKPointAnnotation alloc] init];
        [stationLocation setCoordinate:CLLocationCoordinate2DMake(lat,lng)];
        [stationLocation setTitle:station.frequency];
        [stationLocation setSubtitle:station.ownedBy];
        
        [map addAnnotation:stationLocation];
        
    }
    
    CLLocationDegrees centerLat = (smallestLat + biggestLat) / 2;
    CLLocationDegrees centerLng = (smallestLng + biggestLng) / 2;
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(centerLat, centerLng);
    
    CLLocation *bigCorner = [[CLLocation alloc] initWithLatitude:biggestLat longitude:biggestLng];
    CLLocation *smallCorner = [[CLLocation alloc] initWithLatitude:smallestLat longitude:smallestLng];
    CLLocationDistance distance = [bigCorner distanceFromLocation:smallCorner] * 0.000621371192;
    
    MKCoordinateSpan span = MKCoordinateSpanMake(distance / 69, distance / 69);
    MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
    

    [map setRegion:region animated:YES];
    [map regionThatFits:region];
    
    
}

-(void) viewDidAppear:(BOOL)animated{

    if (map.overlays.count > 0) {
        return;
    }
    
    [self addOverlayFromStationArray];
    
}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay{
    MKCircleView *circleView = [[MKCircleView alloc] initWithCircle:(MKCircle *)overlay];
    
    circleView.fillColor=[[UIColor blueColor] colorWithAlphaComponent:0.1];
    
    if (selectedCircle && selectedCircle.coordinate.latitude == circleView.circle.coordinate.latitude && selectedCircle.coordinate.longitude == circleView.circle.coordinate.longitude) {
        circleView.lineWidth = 2;   
        circleView.strokeColor=[[UIColor blueColor] colorWithAlphaComponent:0.4];
    }
    
    return circleView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    if (![view isKindOfClass:[MKPinAnnotationView class]]) {
        return;
    }
    
    MKPointAnnotation *point = [(MKPinAnnotationView *) view annotation];
    //NSLog(@"point:%lf, %lf",point.coordinate.latitude, point.coordinate.longitude);
    
    for (MKOverlayView *overlay in map.overlays) {
        if ([overlay isKindOfClass:[MKCircle class]]) {
            MKCircle *circle = (MKCircle *) overlay;
            if (circle.coordinate.latitude == point.coordinate.latitude && circle.coordinate.longitude == point.coordinate.longitude) {
                [self setSelectedCircle:circle];
                [map removeOverlay:circle];
                [map addOverlay:circle];
                
//                MKCoordinateSpan span = MKCoordinateSpanMake(circle.radius / 69, circle.radius / 69);
//                MKCoordinateRegion region = MKCoordinateRegionMake(circle.coordinate, span);
//                [map setRegion:region animated:YES];
//                [map regionThatFits:region];
                return;
            }
        }
    }
}

- (void) mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    if (![view isKindOfClass:[MKPinAnnotationView class]]) {
        return;
    }
    
    MKPointAnnotation *point = [(MKPinAnnotationView *) view annotation];

    for (MKOverlayView *overlay in map.overlays) {
        if ([overlay isKindOfClass:[MKCircle class]]) {
            MKCircle *circle = (MKCircle *) overlay;
            if (circle.coordinate.latitude == point.coordinate.latitude && circle.coordinate.longitude == point.coordinate.longitude) {
                [self setSelectedCircle:nil];
                [map removeOverlay:circle];
                [map addOverlay:circle];
            }
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [self setHelpText:[defaults objectForKey:@"mapHelpText"]];
    
    if (helpText.length == 0) {
        [self setHelpText:@"Press and hold to search"];
    }
    
    if ([helpText isEqualToString:@"-"]) {
        [infoLabel setHidden:YES];
    }
    else {
        [infoLabel setText:helpText];
    }
    
    [defaults setObject:helpText forKey:@"mapHelpText"];
    [defaults synchronize];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) shareButtonClicked:(id)sender{
    UIActionSheet *sheet =
    [[UIActionSheet alloc] initWithTitle:
     NSLocalizedString(@"Share stations",
                       @"Title for sheet displayed with options for displaying station information in other applications")
                                delegate:(id)self
                       cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                  destructiveButtonTitle:nil
                       otherButtonTitles:NSLocalizedString(@"Email", @"Email"),NSLocalizedString(@"SMS", @"SMS"), NSLocalizedString(@"Use my location", @"Use my location"),
     nil];
    [sheet showInView:self.view];
    
}

// Called when the user selects an option in the sheet. The sheet will automatically be dismissed.
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc]
                                                           init];
                controller.mailComposeDelegate = (id)self;
                [controller setSubject:@"Radio Station Information"];
                
                NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Station List:\n"];
                
                for (Station *station in stationArray) {
                    [emailBody appendString:[NSString stringWithFormat:@"%@ located in %@\n%@ away\n",station.frequency,station.city,station.distance]];
                }
                
                
                [controller setMessageBody:emailBody isHTML:NO];
                [self presentModalViewController:controller animated:YES];
                
            }
        } break;
        case 1: {
            if ([MFMessageComposeViewController canSendText])
            {
                MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc]
                                                                 init];
                smsController.messageComposeDelegate = (id)self;
                NSMutableString *smsBody = [[NSMutableString alloc] initWithString:@"Stations - "];
                for (Station *station in stationArray) {
                    [smsBody appendFormat:@"%@ in %@",station.frequency,station.city];
                }
                
                [smsController setBody:smsBody];
                [self presentModalViewController:smsController animated:YES];
                
            }
        } break;
        case 2: {
            locationManager = [[CLLocationManager alloc]init];
            locationManager.distanceFilter = kCLDistanceFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
            locationManager.delegate = (id)self;
            [locationManager startUpdatingLocation];
            
            if ([helpText isEqualToString:@"Use action menu to use GPS"]) {
                [self setHelpText:@"-"];
                //[infoLabel setText:helpText];
                [infoLabel setHidden:YES];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:helpText forKey:@"mapHelpText"];
                [defaults synchronize];
            }
        }break;
        default:
            break;
    }
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [self dismissModalViewControllerAnimated:YES];
}

- (void) locationManager: (CLLocationManager *) manager
     didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    //NSLog(@"hit lat:%lf lng:%lf",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    if (locationHitCount < 2) {
        locationHitCount = locationHitCount + 1;
        return;
    }

    [locationManager stopUpdatingLocation];
    [locationManager setDelegate:nil];
    
    [self searchAtLocation:newLocation.coordinate.latitude :newLocation.coordinate.longitude :YES];
    
}

@end
