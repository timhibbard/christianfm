//
//  AppDelegate.h
//  Christian Radio Finder
//
//  Created by Tim Hibbard on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Appirater.h"
#import "iPhoneStreamer.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    UIWindow *window;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, readonly) NSManagedObjectModel *managedObjectModel;   
@property (nonatomic, readonly) NSManagedObjectContext *managedObjectContext;   
@property (nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) iPhoneStreamer *streamer;
@property (nonatomic, retain) NSString *currentlyPlayingUrl;

- (NSString *) applicationDocumentsDirectory;
- (void) playStream: (NSString *) urlString;
- (void) playCurrentStream;
- (void) destroyStream;
- (void) pauseStream;
- (BOOL) isLoadedAndPlaying;
- (BOOL) isLoadedAndPaused;

@end
