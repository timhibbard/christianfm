//
//  AddNewStationViewController.h
//  Christian FM
//
//  Created by Tim Hibbard on 8/29/12.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AddNewStationViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *locationLabel;
@property (nonatomic, retain) IBOutlet UITextField *freqText;
@property (nonatomic, retain) IBOutlet UITextField *callsignText;
@property (nonatomic, retain) IBOutlet UITextField *stationNameText;
@property (nonatomic, retain) IBOutlet UITextField *websiteText;
@property (nonatomic, retain) IBOutlet UITextField *facebookText;
@property (nonatomic, retain) IBOutlet UITextField *twitterText;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSInteger locationHitCount;

-(IBAction)submitButtonClicked:(id)sender;
-(IBAction)cancelButtonClicked:(id)sender;
-(IBAction)backgroundTouched:(id)sender;

@end
