//
//  ReportProblemViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 11/15/12.
//
//

#import "ReportProblemViewController.h"

@interface ReportProblemViewController ()

@end

@implementation ReportProblemViewController{
    NSMutableArray *reasonArray;
    UITextField *noteField;
    BOOL closeMe;
}

@synthesize station, picker;
@synthesize issueCell,noteCell,submitCell,cancelCell;
@synthesize locationHitCount,locationManager;


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return reasonArray.count;
}



- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [reasonArray objectAtIndex:row];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [issueCell.textLabel setText:[reasonArray objectAtIndex:row]];
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.delegate = (id)self;
    [locationManager startUpdatingLocation];
}

- (void) viewDidAppear:(BOOL)animated{
    if (closeMe) {
        [self dismissModalViewControllerAnimated:YES];
        return;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    reasonArray = [[NSMutableArray alloc]init];
    
    [reasonArray addObject:@"Out of range"];
    [reasonArray addObject:@"Not Christian music"];
    [reasonArray addObject:@"Streaming not working"];
    [reasonArray addObject:@"Website not working"];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return issueCell;
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        noteField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 185, 30)];
        noteField.adjustsFontSizeToFitWidth = YES;
        noteField.textColor = [UIColor blackColor];
        noteField.placeholder = @"Click to add note";
        noteField.keyboardType = UIKeyboardTypeDefault;
        noteField.returnKeyType = UIReturnKeyDone;
        noteField.backgroundColor = [UIColor clearColor];
        [noteField setEnabled:YES];
        [noteCell.contentView addSubview:noteField];
        return noteCell;
    }
    if (indexPath.section == 1 && indexPath.row == 0) {
        return submitCell;
    }
    if (indexPath.section == 2 && indexPath.row == 0) {
        return cancelCell;
    }
    
    return nil;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        [noteField resignFirstResponder];
        [picker setHidden:NO];
        if ([issueCell.textLabel.text isEqualToString:@"Click to select issue"]) {
            issueCell.textLabel.text = @"Out of range";
        }
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        [picker setHidden:YES];
        //return noteCell;
    }
    if (indexPath.section == 1 && indexPath.row == 0) {
        [noteField resignFirstResponder];
        [picker setHidden:YES];
        [self emailIssue];
        
        //return submitCell;
    }
    if (indexPath.section == 2 && indexPath.row == 0) {
        [noteField resignFirstResponder];
        [picker setHidden:YES];
        [self dismissModalViewControllerAnimated:YES];
    }
}

- (void) locationManager: (CLLocationManager *) manager
     didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    //NSLog(@"hit lat:%lf lng:%lf",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    if (locationHitCount < 1) {
        locationHitCount = locationHitCount + 1;
        return;
    }
    
    //[locationLabel setText:[NSString stringWithFormat:@"%.3lf %.3lf", newLocation.coordinate.latitude, newLocation.coordinate.longitude]];
    [locationManager stopUpdatingLocation];
    [locationManager setDelegate:nil];
    
}

- (void) emailIssue{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc]
                                                   init];
        controller.mailComposeDelegate = (id)self;
        NSArray *to = [[NSArray alloc] initWithObjects:@"timhibbard@gmail.com",nil];
        [controller setToRecipients:to];
        [controller setSubject:[NSString stringWithFormat:@"CRLApp issue %@",issueCell.textLabel.text]];
        
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"Issue with %@\n", station.callSign]];
        [emailBody appendString:[NSString stringWithFormat:@"Name: %@\n",station.ownedBy]];
        [emailBody appendString:[NSString stringWithFormat:@"Note: %@\n",noteField.text]];
        if ([issueCell.textLabel.text isEqualToString:@"Website not working"]) {
            [emailBody appendString:[NSString stringWithFormat:@"Website: %@\n",station.website]];
        }
        if ([issueCell.textLabel.text isEqualToString:@"Streaming not working"]) {
            [emailBody appendString:[NSString stringWithFormat:@"Streaming: %@\n",station.streamUrl]];
            [emailBody appendString:[NSString stringWithFormat:@"Background streaming: %@\n",station.backgroundStreamUrl]];
        }
        if ([issueCell.textLabel.text isEqualToString:@"Out of range"]) {
            [emailBody appendString:[NSString stringWithFormat:@"User's location: %.4lf, %.4lf\n",locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude]];
            [emailBody appendString:[NSString stringWithFormat:@"Distance from tower: %@\n",station.distance]];
            [emailBody appendString:[NSString stringWithFormat:@"Expected Range: %@\n",station.range]];
        }
        
        [emailBody appendString:[NSString stringWithFormat:@"DatabaseID: %@\n",station.databaseId]];

        
        [controller setMessageBody:emailBody isHTML:NO];
        [self presentModalViewController:controller animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This device cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
        {
            closeMe = YES;
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thanks!" message:@"Issue submitted. I really appreciate it!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //[alert show];
            //[self.navigationController popToRootViewControllerAnimated:YES];
            
        }
			break;
		case MFMailComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email did not send" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
			break;
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
    
}


@end
