//
//  StationDetailViewController.h
//  Christian FM
//
//  Created by Tim Hibbard on 8/4/12.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Station.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Appirater.h"

@class Station;

@interface MapDetailViewController : UIViewController<MKMapViewDelegate>
{
    
}

@property (nonatomic, strong) Station *station;
@property (nonatomic, retain) IBOutlet UILabel *distanceLabel;
@property (nonatomic, retain) IBOutlet MKMapView *map;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic, retain) IBOutlet UIView *displayView;
@property (nonatomic) BOOL gotFirstHit;
@property (nonatomic) BOOL gotInitalViewport;

- (IBAction)shareButtonClicked:(id)sender;

@end
