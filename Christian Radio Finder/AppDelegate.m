//
//  AppDelegate.m
//  Christian Radio Finder
//
//  Created by Tim Hibbard on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Station.h"
#import "StationTableViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize managedObjectModel;
@synthesize managedObjectContext;
@synthesize persistentStoreCoordinator;
@synthesize streamer, currentlyPlayingUrl;

- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    [self createEditableCopyOfDatabaseIfNeeded];
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"ChristianRadioFinder.sqlite"]];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                 configuration:nil URL:storeUrl options:options error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Override point for customization after application launch.
    [Appirater setAppId:@"545689326"];
    //[Appirater setDebug:YES];

    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
//    StationTableViewController *controller = (StationTableViewController *)self.window.rootViewController;
    
    
    
    StationTableViewController *controller = (StationTableViewController *)[[navigationController viewControllers] objectAtIndex:0];
    controller.managedObjectContext = self.managedObjectContext;
    [self createEditableCopyOfDatabaseIfNeeded];
    [Appirater appLaunched:YES];
    return YES;


}

-(void) applicationDidReceiveMemoryWarning:(UIApplication *)application{
    NSLog(@"got memory warning");
}


// Creates a writable copy of the bundled default database in the application Documents directory.
- (void)createEditableCopyOfDatabaseIfNeeded {
    
    return;
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"ChristianRadioFinder.sqlite"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success)
        //uncomment line to overwrite
        //[fileManager removeItemAtPath:writableDBPath error:&error];
        return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ChristianRadioFinder.sqlite"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [Appirater appEnteredForeground:YES];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)theEvent{
    if (theEvent.type == UIEventTypeRemoteControl) {
        
        switch (theEvent.subtype) {
                
            case UIEventSubtypeRemoteControlTogglePlayPause:{
                if (!streamer) {
                    return;
                }
                if (streamer.isWaiting) {
                    return;
                }
                if (streamer.isPlaying) {
                    [self pauseStream];
                    return;
                }
                [self playCurrentStream];
            }break;
            default:
                break;
        }
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void) playCurrentStream{
    [self playStream:currentlyPlayingUrl];
}

- (void) playStream: (NSString *) urlString{
    if ([urlString isEqualToString:currentlyPlayingUrl]) {
        if (streamer.isWaiting || streamer.isPlaying) {
            return;
        }
        [streamer play];
        return;
    }

    [self destroyStream];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    streamer = (iPhoneStreamer*)[iPhoneStreamer streamWithURL:url];
    
    BOOL startResult = [streamer start];
    if (startResult) {
        [self setCurrentlyPlayingUrl:urlString];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(stateChanged:)
         name:ASStatusChangedNotification
         object:streamer];
        
        
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        [self becomeFirstResponder];
    }
    
}

- (void) destroyStream{
    if (!streamer) {
        return;
    }
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:ASStatusChangedNotification
     object:streamer];
    [streamer stop];
    streamer = nil;
    currentlyPlayingUrl = [NSString alloc];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}

- (BOOL) isLoadedAndPaused{
    if (!streamer) {
        return NO;
    }
    if (streamer.isPaused) {
        return YES;
    }
    return NO;
}

- (BOOL) isLoadedAndPlaying{
    if (!streamer) {
        return NO;
    }
    if (streamer.isPlaying || streamer.isWaiting) {
        return YES;
    }
    return NO;
}

- (void) pauseStream{
    if (!streamer) {
        return;
    }
    if (streamer.isWaiting || streamer.isPaused) {
        return;
    }
    [streamer pause];
}

- (void) stateChanged:(NSNotification*) not {
    AudioStreamer *stream = [not object];
    if ([stream errorCode] != AS_NO_ERROR) {
        // handle the error via a UI, retrying the stream, etc.
    } else if ([stream isPlaying]) {
        
    } else if ([stream isPaused]) {

    } else if ([stream isDone]) {

    } else {
        // stream is waiting for data, probably nothing to do
    }
}



@end
