//
//  StationDetailTableViewController.h
//  Christian FM
//
//  Created by Tim Hibbard on 8/24/12.
//
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Station.h"
#import "AppDelegate.h"
#import "ReportProblemViewController.h"

@class Station;

@interface StationDetailTableViewController : UITableViewController

@property (nonatomic, strong) Station *station;
@property (nonatomic, retain) IBOutlet UITableViewCell *freqCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *ownedByCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *streamingCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *twitterCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *facebookCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *geoCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *reminderCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *favoriteCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *reportProblemCell;
@property (nonatomic) NSString *eventId;
@property (nonatomic) BOOL providedStoreAccess;
@property (nonatomic) EKEventStore *eventStore;




- (IBAction)shareButtonClicked:(id)sender;

@end
