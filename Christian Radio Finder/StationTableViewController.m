//
//  StationTableViewController_iPhone.m
//  Christian FM
//
//  Created by Tim Hibbard on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StationTableViewController.h"


@interface StationTableViewController ()

@end

@implementation StationTableViewController{
    NSArray *defaultToolBarItems;
    NSMutableArray *goodStations;
    NSMutableArray *fringeStations;
    NSMutableArray *outOfRangeStations;
    NSMutableArray *tableSections;
}
@synthesize managedObjectContext;
@synthesize stationArray;
@synthesize locationManager;
@synthesize pullTableView;
@synthesize selectedRowIndex;
@synthesize selectionHasBeenMade;
@synthesize rangeDelta;
@synthesize displayInDetails;
@synthesize loadMore;
@synthesize helpText;


#define kCellHeight 44.0

#pragma mark - Action Sheet and Sharing

- (void) shareToolbarClick:(id)sender{
    if (![MFMailComposeViewController canSendMail] && ![MFMessageComposeViewController canSendText]) {
        //can't do anything...get out
        return;
    }
    
    if (![MFMessageComposeViewController canSendText]) {
        //no text...send mail
        [self sendMail];
    }
    
    if (![MFMailComposeViewController canSendMail]) {
        //no mail...send text
        [self sendText];
    }
    
    //both..ask user
    UIActionSheet *sheet =
    [[UIActionSheet alloc] initWithTitle:
     NSLocalizedString(@"Share stations via",
                       @"Title for sheet displayed with options for displaying station information in other applications")
                                delegate:(id)self
                       cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                  destructiveButtonTitle:nil
                       otherButtonTitles:NSLocalizedString(@"Email", @"Email"),NSLocalizedString(@"SMS", @"SMS"), nil];
    //[sheet showInView:self.view];
    //[sheet showFromBarButtonItem:sender animated:YES];
    [sheet showFromToolbar:self.navigationController.toolbar];
    
}

- (void) sendText{
    MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc]
                                                     init];
    smsController.messageComposeDelegate = (id)self;
    NSMutableString *smsBody = [[NSMutableString alloc] initWithString:@"Stations - "];
    for (Station *station in stationArray) {
        [smsBody appendFormat:@"%@ in %@",station.frequency,station.city];
    }
    
    [smsController setBody:smsBody];
    [self presentModalViewController:smsController animated:YES];
}

- (void) sendMail{
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc]
                                               init];
    controller.mailComposeDelegate = (id)self;
    [controller setSubject:@"Radio Station Information"];
    
    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Station List:\n"];
    
    for (Station *station in stationArray) {
        [emailBody appendString:[NSString stringWithFormat:@"%@ (%@) located in %@\n%@ away\n",station.frequency,station.ownedBy,station.city,station.distance]];
    }
    
    
    [controller setMessageBody:emailBody isHTML:NO];
    [self presentModalViewController:controller animated:YES];
}

// Called when the user selects an option in the sheet. The sheet will automatically be dismissed.
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            if ([MFMailComposeViewController canSendMail])
            {
                [self sendMail];
                
            }
        } break;
        case 1: {
            if ([MFMessageComposeViewController canSendText])
            {
                [self sendText];
                
            }
        } break;
        case 2:{
            //[self performSegueWithIdentifier:@"NewStationSegue" sender:self];
        }break;
        case 3:{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"Press and hold to search" forKey:@"mapHelpText"];
            [defaults setObject:@"Swipe left to see more info" forKey:@"helpText"];
            [defaults synchronize];
            [self setHelpText:@"Swipe left to see more info"];
            [self refreshTableView];
            
        } break;
        default:
            break;
    }
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - refreshing

- (void) refreshTable{
    /*
     
     Code to actually refresh goes here.
     
     */
    
    rangeDelta = 0;
    
    [self fetchByLocation];
    
}

- (void) loadMoreDataToTable{
    /*
     
     Code to actually load more data goes here.
     
     */
    if (!rangeDelta) {
        rangeDelta = 0;
    }
    
    rangeDelta = rangeDelta + 5;
    [self fetchByLocation];
    
    
}

- (void) refreshToolbarClick:(id)sender{
    if(!self.pullTableView.pullTableIsRefreshing) {
        self.pullTableView.pullTableIsRefreshing = YES;
        [self performSelector:@selector(refreshTable) withObject:nil afterDelay:.1];
    }
}

- (void) fetchByLocation{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.delegate = (id)self;
    [locationManager startUpdatingLocation];
}


- (void) locationManager: (CLLocationManager *) manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    double GPSlat, GPSlng;
    GPSlat = locationManager.location.coordinate.latitude;
    GPSlng = locationManager.location.coordinate.longitude;
    if (GPSlat == 0) {
        return;
    }
    
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    
    StationLookupManager *stationLookupManager = [[StationLookupManager alloc] init];
    
    [stationLookupManager getData:GPSlat :GPSlng :rangeDelta :^(NSString *errorMessage) {
        NSLog(@"%@",errorMessage);
        UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorView show];
    } :^(NSMutableArray *xmlStationArray) {
        NSMutableArray *bindableArray = [[NSMutableArray alloc]init];
        goodStations = [[NSMutableArray alloc]init];
        fringeStations = [[NSMutableArray alloc]init];
        outOfRangeStations = [[NSMutableArray alloc]init];
        for (int a = 0; a < [xmlStationArray count]; a++) {
            Station *newStation = (Station *)[xmlStationArray objectAtIndex:a];
            CLLocationDegrees lat = [newStation.lat doubleValue];
            CLLocationDegrees lng = [newStation.lng doubleValue];
            CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            CLLocationDistance distance = [newLocation distanceFromLocation:radioLocation] * 0.000621371192;
            [newStation setSignalStrength:[[NSDecimalNumber alloc] initWithDouble:((newStation.range.doubleValue - distance)/newStation.range.doubleValue)] ];
            NSLog(@"strength=%@", newStation.signalStrength);
            
            [newStation setDistance:[NSString stringWithFormat:@"%d miles", (int) ceil(distance)]];
            if (distance <= [newStation.range doubleValue] + rangeDelta) {
                //[xmlStationArray replaceObjectAtIndex:a withObject:newStation];
                [bindableArray addObject:newStation];
                if ([newStation.signalStrength doubleValue] >= .25) {
                    [goodStations addObject:newStation];
                    NSLog(@"added %@ to good",newStation.ownedBy);
                }
                else if ([newStation.signalStrength doubleValue] > 0){
                    [fringeStations addObject:newStation];
                    NSLog(@"added %@ to fringe",newStation.ownedBy);
                }
                else{
                    [outOfRangeStations addObject:newStation];
                    NSLog(@"added %@ to outofrange",newStation.ownedBy);
                }
                    
            }
            
        }
        [self setStationArray:bindableArray];
    }];
    
//    [stationArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//        Station *st1 = (Station *)obj1;
//        Station *st2 = (Station *)obj2;
//        
//        return [st2.signalStrength compare:st1.signalStrength];
//        
//    }];
    
    [self sortBySignalStrength:stationArray];
    [self sortBySignalStrength:goodStations];
    [self sortBySignalStrength:fringeStations];
    [self sortBySignalStrength:outOfRangeStations];
    

    
    
    self.pullTableView.pullLastRefreshDate = [NSDate date];
    self.pullTableView.pullTableIsRefreshing = NO;
    self.pullTableView.pullTableIsLoadingMore = NO;
    [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:YES];
    
}

- (void) sortBySignalStrength: (NSMutableArray *) array{
    if (!array || array.count == 0) {
        return;
    }
    [array sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Station *st1 = (Station *)obj1;
        Station *st2 = (Station *)obj2;
        
        return [st2.signalStrength compare:st1.signalStrength];
        
    }];
}

- (void) refreshTableView{
    [self.tableView reloadData];
    //[self.pullTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    self.tableView.contentOffset = CGPointMake(0, self.searchBar.frame.size.height);
}

- (void) locationManager: (CLLocationManager *) manager didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    self.pullTableView.pullLastRefreshDate = [NSDate date];
    self.pullTableView.pullTableIsRefreshing = NO;
    self.pullTableView.pullTableIsLoadingMore = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"locationManager didFailWithError:%@",error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GPS Error" message:@"Could not get GPS info. Please enable if disabled then pull to refresh." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    //throw up a message
}

#pragma mark - Core Stuff

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([[segue identifier] isEqualToString:@"StationDetailSegue"]) {
        
        NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
        StationDetailTableViewController *detailViewController = [segue destinationViewController];
        [detailViewController setHidesBottomBarWhenPushed:YES];
        NSInteger row = 0;
        switch (selectedRow.section) {
            case 0:
                row = selectedRow.row;
                break;
            case 1:
                row = [self.tableView numberOfRowsInSection:0] + selectedRow.row;
                break;
            case 2:
                row = [self.tableView  numberOfRowsInSection:0] + [self.tableView  numberOfRowsInSection:1] + selectedRow.row;
                break;
            default:
                break;
        }
        detailViewController.station = [stationArray objectAtIndex:row];
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"ShowMapFromLongPressSegue"]) {
        NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
        MapDetailViewController *mapController = [segue destinationViewController];
        [mapController setHidesBottomBarWhenPushed:YES];
        NSInteger row = 0;
        switch (selectedRow.section) {
            case 0:
                row = selectedRow.row;
                break;
            case 1:
                row = [self.tableView  numberOfRowsInSection:0] + selectedRow.row;
                break;
            case 2:
                row = [self.tableView  numberOfRowsInSection:0] + [self.tableView  numberOfRowsInSection:1] + selectedRow.row;
                break;
            default:
                break;
        }
        mapController.station = [stationArray objectAtIndex:selectedRowIndex];
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"ViewAllOnMapSegue"]) {
        StationMapViewController *mapController = [segue destinationViewController];
        [mapController setStationArray:[self stationArray]];
        [mapController setHidesBottomBarWhenPushed:YES];
        return;
    }
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:NO];
}

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)becomeActive:(NSNotification *)notification {
    // only respond if the selected tab is our current tab
    if ([self.navigationController.visibleViewController isKindOfClass:[StationTableViewController class]]) {
        NSLog(@"view conroller = %@",self.navigationController.visibleViewController);
        
        
        NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:self.pullTableView.pullLastRefreshDate];
        NSLog(@"Diff = %lf",diff);
        if(!self.pullTableView.pullTableIsRefreshing && ((diff / 60) > 5)) {
            //self.pullTableView.pullTableIsRefreshing = YES;
            [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1];
        }
    }
    
    
}

-(void)didChangeSegmentControl:(UISegmentedControl*)control{
    
    NSString *title =[control titleForSegmentAtIndex:control.selectedSegmentIndex];
    NSLog(@"title : %@",title);
    
    
}

- (void)viewDidUnload{
    
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

- (void) viewDidLoad{
    [super viewDidLoad];
    //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    //self.navigationController.view.frame = [[UIScreen mainScreen] applicationFrame];
    selectionHasBeenMade = NO;
    
    
    if(!self.pullTableView.pullTableIsRefreshing) {
        self.pullTableView.pullTableIsRefreshing = YES;
        [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(becomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    //UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]
    //                                                  initWithTarget:self action:@selector(longPress:)] ;
    //[cell addGestureRecognizer:longPressGesture];
    
    UISwipeGestureRecognizer *rightSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    [rightSwipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
    UISwipeGestureRecognizer *leftSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    [leftSwipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    
    UISwipeGestureRecognizer *upSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUp:)];
    [upSwipeGesture setDirection:UISwipeGestureRecognizerDirectionUp];
    [upSwipeGesture setNumberOfTouchesRequired:2];
    [upSwipeGesture setDelegate:self];
    
    UISwipeGestureRecognizer *downSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDown:)];
    [downSwipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [downSwipeGesture setNumberOfTouchesRequired:2];
    [downSwipeGesture setDelegate:self];
    
    [self.pullTableView addGestureRecognizer:rightSwipeGesture];
    [self.pullTableView addGestureRecognizer:leftSwipeGesture];
    [self.pullTableView addGestureRecognizer:upSwipeGesture];
    [self.pullTableView addGestureRecognizer:downSwipeGesture];
    
    [self setHelpText:@"Swipe left to see more info"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self setHelpText:[defaults objectForKey:@"helpText"]];
    if (helpText.length == 0) {
        [self setHelpText:@"Swipe left to see more info"];
    }
    
    [defaults setObject:helpText forKey:@"helpText"];
    [defaults synchronize];
    
}

- (void) viewDidAppear:(BOOL)animated{
    [self constructToolbar];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self constructToolbar];
}

#pragma mark - Table View

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    NSLog(@"getting number of sections");
    if (self.pullTableView.pullTableIsRefreshing == YES) {
        NSLog(@"returning 0 sections");
        return 0;
    }
    
    tableSections = [[NSMutableArray alloc]init];
    if (goodStations.count > 0) {
        [tableSections addObject:@"Close by"];
    }
    if (fringeStations.count > 0) {
        [tableSections addObject:@"Fringe signal"];
    }
    if (outOfRangeStations.count > 0) {
        [tableSections addObject:@"Out of range"];
    }
    if ([helpText isEqualToString:@"-"] == NO) {
        [tableSections addObject:@"Tips"];
    }
    [tableSections addObject:@""];
    NSLog(@"returning %i sections",tableSections.count);
    return tableSections.count;
    
}

- (CAGradientLayer *) greyGradient {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.startPoint = CGPointMake(0.5, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    
    UIColor *color1 = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0];
    UIColor *color2 = [UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0];
    
    [gradient setColors:[NSArray arrayWithObjects:(id)color1.CGColor, (id)color2.CGColor, nil]];
    return gradient;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 36.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel * label = [[UILabel alloc] init];
    label.textColor = [UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"   %@",[tableSections objectAtIndex:section]];
    label.font = [UIFont boldSystemFontOfSize:19.0f];
    label.backgroundColor = [tableView backgroundColor];
    label.shadowOffset = CGSizeMake(1, 1);
    return label;
    
    CGFloat width = CGRectGetWidth(tableView.bounds);
    CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0,0,width,height)];
    container.layer.borderColor = [UIColor grayColor].CGColor;
    container.layer.borderWidth = 1.0f;
    CAGradientLayer *gradient = [self greyGradient];
    gradient.frame = container.bounds;
    [container.layer addSublayer:gradient];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,0,width,height)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font= [UIFont boldSystemFontOfSize:19.0f];
    headerLabel.shadowOffset = CGSizeMake(1, 1);
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.shadowColor = [UIColor darkGrayColor];
    headerLabel.text = [tableSections objectAtIndex:section];
    return headerLabel;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [tableSections objectAtIndex:section];
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.pullTableView.pullTableIsRefreshing) {
        return 0;
    }
    
    NSString *header = [tableSections objectAtIndex:section];
    
    if ([header isEqualToString:@""]) {
        return 1;
    }
    if ([header isEqualToString:@"Tips"]) {
        return 1;
    }
    if ([header isEqualToString:@"Close by"]) {
        return goodStations.count;
    }
    if ([header isEqualToString:@"Fringe signal"]) {
        return fringeStations.count;
    }
    if ([header isEqualToString:@"Out of range"]) {
        return outOfRangeStations.count;
    }
    
    return 0;

}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCellHeight;
}

- (Station *) getStationFromIndexPath: (NSIndexPath *) indexPath{
    //NSLog(@"getting station for section %i row %i",indexPath.section, indexPath.row);
    
    if (self.pullTableView.pullTableIsRefreshing) {
        return nil;
    }
    
    NSString *header = [tableSections objectAtIndex:indexPath.section];
    
    if ([header isEqualToString:@"Close by"]) {
        return [goodStations objectAtIndex:indexPath.row];
    }
    if ([header isEqualToString:@"Fringe signal"]) {
        return [fringeStations objectAtIndex:indexPath.row];
    }
    if ([header isEqualToString:@"Out of range"]) {
        return [outOfRangeStations objectAtIndex:indexPath.row];
    }
    return nil;
    

}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"getting cell for section %i row %i",indexPath.section, indexPath.row);
    static NSString *cellIdentifier = @"StationCell";
    static NSDateFormatter *dateFormatter = nil;
    
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"h:mm.ss a"];
    }
    
        
    StationCell *cell = (StationCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[StationCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(longPress:)];
    [cell addGestureRecognizer:longPressGesture];
    
    if (indexPath.section == tableView.numberOfSections - 1) {
        [cell.textLabel setFont:[UIFont systemFontOfSize:17]];
        [cell.textLabel setText:[NSString stringWithFormat:@"Extend search by %d miles",rangeDelta + 5]];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell.textLabel setTextAlignment:UITextAlignmentCenter];
        [cell.frequencyLabel setText:@""];
        [cell.cityLabel setText:@""];
        return cell;
    }

    
    Station *station = [self getStationFromIndexPath:indexPath];
    
    if (!station) {
        [cell.textLabel setFont:[UIFont italicSystemFontOfSize:17]];
        [cell.textLabel setText:helpText];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell.textLabel setTextAlignment:UITextAlignmentCenter];
        [cell.frequencyLabel setText:@""];
        [cell.cityLabel setText:@""];
        return cell;
    }

    
    [cell.textLabel setText:@""];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    cell.frequencyLabel.text = station.frequency;
    if ((UIDevice.currentDevice.orientation == UIInterfaceOrientationLandscapeLeft || UIDevice.currentDevice.orientation == UIInterfaceOrientationLandscapeRight) && station.streamUrl.length > 0) {
        cell.frequencyLabel.text = [NSString stringWithFormat:@"%@ (Streaming available)",station.frequency];
        
    }
    if (station.ownedBy.length > 0) {
        cell.cityLabel.text = station.ownedBy;
    }
    else{
        cell.cityLabel.text = station.city;
    }
    
    switch (displayInDetails) {
        case Owner:
            break;
        case Distance:
            cell.cityLabel.text = [NSString stringWithFormat:@"%@ away",station.distance];
            break;
        case City:
            cell.cityLabel.text = station.city;
            break;
        case Range:
            cell.cityLabel.text = [NSString stringWithFormat:@"%i mile range", (int)[station.range doubleValue]];
            break;
        case Strength:
        {
            int displayStrength = (int)(station.signalStrength.doubleValue * 100);
            if (displayStrength < 0) {
                displayStrength = 0;
            }
            NSString *friendlyStrength;
            if (displayStrength >= 75) {
                friendlyStrength = @"Excellent";
            }
            if (displayStrength >= 50 && displayStrength < 75) {
                friendlyStrength = @"Great";
            }
            if (displayStrength >= 35 && displayStrength < 50) {
                friendlyStrength = @"Good";
            }
            if (displayStrength >= 10 && displayStrength < 35) {
                friendlyStrength = @"Poor";
            }
            if (displayStrength < 10) {
                friendlyStrength = @"Fringe";
            }
            if (displayStrength == 0) {
                friendlyStrength = @"No";
            }
            cell.cityLabel.text = [NSString stringWithFormat:@"%@ signal (%i%%)",friendlyStrength, displayStrength ];
            break;
        }
        default:
            break;
    }
    
    
    return cell;
}

#pragma mark - Searching

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [searchBar resignFirstResponder];
    [self searchStations:searchBar.text];

}

-(void) searchStations: (NSString *) stationName {
    
    
    StationLookupManager *stationLookupManager = [[StationLookupManager alloc] init];
    
    [stationLookupManager getData: stationName :^(NSString *errorMessage) {
        NSLog(@"%@",errorMessage);
        UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [errorView show];
    } :^(NSMutableArray *xmlStationArray) {
        NSMutableArray *bindableArray = [[NSMutableArray alloc]init];
        for (int a = 0; a < [xmlStationArray count]; a++) {
            Station *newStation = (Station *)[xmlStationArray objectAtIndex:a];
            CLLocationDegrees lat = [newStation.lat doubleValue];
            CLLocationDegrees lng = [newStation.lng doubleValue];
            CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            CLLocationDistance distance = [locationManager.location distanceFromLocation:radioLocation] * 0.000621371192;
            [newStation setSignalStrength:[[NSDecimalNumber alloc] initWithDouble:((newStation.range.doubleValue - distance)/newStation.range.doubleValue)] ];
            //NSLog(@"strength=%@", newStation.signalStrength);
            
            [newStation setDistance:[NSString stringWithFormat:@"%d miles", (int) ceil(distance)]];
            [bindableArray addObject:newStation];
            
        }
        [self setStationArray:bindableArray];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
    
    [stationArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Station *st1 = (Station *)obj1;
        Station *st2 = (Station *)obj2;
        
        return [st2.signalStrength compare:st1.signalStrength];
        
    }];
    
    
    
    
    
    [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:YES];
    self.pullTableView.pullLastRefreshDate = [NSDate date];
    self.pullTableView.pullTableIsRefreshing = NO;
    self.pullTableView.pullTableIsLoadingMore = NO;
    
}


#pragma mark - Gestures

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}


- (IBAction) longPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    StationCell *cell = (StationCell *)[gestureRecognizer view];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath.section == self.tableView.numberOfSections - 1) {
        [cell.textLabel setText:[NSString stringWithFormat:@"Extend search by %d miles",rangeDelta + 20]];
        rangeDelta = rangeDelta + 15;
        if (self.pullTableView.pullTableIsLoadingMore == NO) {
            self.pullTableView.pullTableIsLoadingMore = YES;
            [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:0.1f];
        }
        return;
    }
    [self setSelectedRowIndex:indexPath.row];
    //NSLog(@"long press at index:%i",indexPath.row);
    if ([helpText isEqualToString:@"Press and hold station to view map"]) {
        [self setHelpText:@"-"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:helpText forKey:@"helpText"];
        [defaults synchronize];
        [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:NO];
    }
    [self performSegueWithIdentifier:@"ShowMapFromLongPressSegue" sender:self];
    
    
}

- (IBAction)swipeRight:(UISwipeGestureRecognizer *)swipeRightRecognizer{
    //NSLog(@"swipe right");
    if (!displayInDetails) {
        displayInDetails = 0;
    }
    
    if (displayInDetails - 1 == -1) {
        displayInDetails = 5;
    }
    
    displayInDetails = displayInDetails - 1;
    if ([helpText isEqualToString:@"Swipe right to cycle backwards"]) {
        [self setHelpText:@"Press and hold station to view map"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:helpText forKey:@"helpText"];
        [defaults synchronize];
    }
    
    [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:NO];
    
}

- (IBAction)swipeUp:(UISwipeGestureRecognizer *)swipeUpRecognizer{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    //[viewController.view setFrame: [viewController.view bounds]];
    //[self.view setFrame:[self.view bounds]];
    self.navigationController.view.frame = [[UIScreen mainScreen] applicationFrame];
}

- (IBAction)swipeDown:(UISwipeGestureRecognizer *)swipeDownRecognizer{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    self.navigationController.view.frame = [[UIScreen mainScreen] applicationFrame];
    
}

- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)swipeLeftRecognizer{
    //NSLog(@"swipe left");
    if (!displayInDetails) {
        displayInDetails = 0;
    }
    
    if (displayInDetails + 1 == 5) {
        displayInDetails = -1;
    }
    
    displayInDetails = displayInDetails + 1;
    
    if ([helpText isEqualToString:@"Swipe left again to see more"]) {
        [self setHelpText:@"Swipe right to cycle backwards"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:helpText forKey:@"helpText"];
        [defaults synchronize];
    }
    if ([helpText isEqualToString:@"Swipe left to see more info"]) {
        [self setHelpText:@"Swipe left again to see more"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:helpText forKey:@"helpText"];
        [defaults synchronize];
    }
    [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:NO];
}



#pragma mark - Toolbar

- (void) constructToolbar{
    if (!defaultToolBarItems) {
        defaultToolBarItems = self.navigationController.toolbar.items;
    }
    
    BOOL playing = [(AppDelegate *)[[UIApplication sharedApplication]delegate ] isLoadedAndPlaying];
    BOOL paused = [(AppDelegate *)[[UIApplication sharedApplication]delegate ] isLoadedAndPaused];
    
    if (!playing && !paused) {
        NSMutableArray *items = [NSMutableArray arrayWithArray:defaultToolBarItems];
        [items removeObjectAtIndex:6];
        [items removeObjectAtIndex:5];
        [items removeObjectAtIndex:4];
        [items removeObjectAtIndex:3];
        [self.navigationController.toolbar setItems:[items copy] animated:NO];
        return;
    }
    if (playing) {
        NSMutableArray *items = [NSMutableArray arrayWithArray:defaultToolBarItems];
        [items removeObjectAtIndex:4];
        [items removeObjectAtIndex:3];
        [self.navigationController.toolbar setItems:[items copy] animated:NO];
        return;
    }
    if (paused) {
        NSMutableArray *items = [NSMutableArray arrayWithArray:defaultToolBarItems];
        [items removeObjectAtIndex:6];
        [items removeObjectAtIndex:5];
        [self.navigationController.toolbar setItems:[items copy] animated:NO];
        return;
    }
}

- (void) playToolbarClick:(id)sender{
    [(AppDelegate *)[[UIApplication sharedApplication]delegate ] playCurrentStream];
    [self constructToolbar];
}

- (void) pauseToolbarClick:(id)sender{
    [(AppDelegate *)[[UIApplication sharedApplication]delegate ] pauseStream];
    [self constructToolbar];
}



#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView{
    
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:0.1f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:0.5f];
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    if (indexPath.section == tableView.numberOfSections - 1) {
        //[self loadMoreDataToTable];
        if (self.pullTableView.pullTableIsLoadingMore == NO) {
            self.pullTableView.pullTableIsLoadingMore = YES;
            [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:0.1f];
        }
    }
    else if (![self getStationFromIndexPath:indexPath]){
        //do nada
        NSLog(@"user selected tip");
    }
    else{
        [self performSegueWithIdentifier:@"StationDetailSegue" sender:tableView];
    }
        


    if (selectionHasBeenMade && selectedRowIndex == indexPath.row) {
        selectionHasBeenMade = NO;
        selectedRowIndex = 0;
    }
    else {
        selectionHasBeenMade = YES;
        selectedRowIndex = indexPath.row;
    }
    
    

    //[tableView deselectRowAtIndexPath:indexPath animated:TRUE];


    // This is where magic happens...
    [tableView beginUpdates];
    [tableView endUpdates];
    
}



@end
