//
//  AddNewStationViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 8/29/12.
//
//

#import "AddNewStationViewController.h"

@interface AddNewStationViewController ()

@end

@implementation AddNewStationViewController

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;


@synthesize locationLabel;
@synthesize freqText;
@synthesize callsignText;
@synthesize stationNameText;
@synthesize websiteText;
@synthesize facebookText;
@synthesize twitterText;
@synthesize locationManager;
@synthesize locationHitCount;

CGFloat animatedDistance;
CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //from:
    //http://cocoawithlove.com/2008/10/sliding-uitextfields-around-to-avoid.html
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =[self.view.window convertRect:self.view.bounds fromView:self.view];
    //So now we have the bounds, we need to calculate the fraction between the top and bottom of the middle section for the text field's midline:
        
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    //Clamp this fraction so that the top section is all "0.0" and the bottom section is all "1.0".
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    //Now take this fraction and convert it into an amount to scroll by multiplying by the keyboard height for the current screen orientation. Notice the calls to floor so that we only scroll by whole pixel amounts.
        
        UIInterfaceOrientation orientation =
        [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    //Finally, apply the animation. Note the use of setAnimationBeginsFromCurrentState: — this will allow a smooth transition to new text field if the user taps on another.
        
        CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}

- (void) cancelButtonClicked:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}


- (void) submitButtonClicked:(id)sender{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc]
                                                   init];
        controller.mailComposeDelegate = (id)self;
        NSArray *to = [[NSArray alloc] initWithObjects:@"timhibbard@gmail.com",nil];
        [controller setToRecipients:to];
        [controller setSubject:@"New Christian Station"];
        
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"New Station Info:\n"];
        [emailBody appendFormat:[NSString stringWithFormat:@"Freq: %@\n",freqText.text]];
        [emailBody appendString:[NSString stringWithFormat:@"Location: %@\n",locationLabel.text]];
        [emailBody appendString:[NSString stringWithFormat:@"Callsign: %@\n",callsignText.text]];
        [emailBody appendString:[NSString stringWithFormat:@"Name: %@\n",stationNameText.text]];
        [emailBody appendString:[NSString stringWithFormat:@"Website: %@\n",websiteText.text]];
        [emailBody appendString:[NSString stringWithFormat:@"Facebook: %@\n",facebookText.text]];
        [emailBody appendString:[NSString stringWithFormat:@"Twitter: %@\n",twitterText.text]];
        
        [controller setMessageBody:emailBody isHTML:NO];
        [self presentModalViewController:controller animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This device cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thanks!" message:@"Station submitted. I really appreciate it!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            //[self.navigationController popToRootViewControllerAnimated:YES];
        }
			break;
		case MFMailComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email did not send" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
			break;
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    NSLog(@"Device is: %@",[UIDevice currentDevice].model);
    locationManager = [[CLLocationManager alloc]init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.delegate = (id)self;
    [locationManager startUpdatingLocation];
}

- (void) locationManager: (CLLocationManager *) manager
     didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    //NSLog(@"hit lat:%lf lng:%lf",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    if (locationHitCount < 1) {
        locationHitCount = locationHitCount + 1;
        return;
    }
    
    [locationLabel setText:[NSString stringWithFormat:@"%.3lf %.3lf", newLocation.coordinate.latitude, newLocation.coordinate.longitude]];
    [locationManager stopUpdatingLocation];
    [locationManager setDelegate:nil];
    
}

-(void)backgroundTouched:(id)sender{
    [freqText resignFirstResponder];
    [callsignText resignFirstResponder];
    [stationNameText resignFirstResponder];
    [websiteText resignFirstResponder];
    [facebookText resignFirstResponder];
    [twitterText resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
        [self submitButtonClicked:self];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([UIDevice.currentDevice.model isEqualToString:@"iPad"]) {
        LANDSCAPE_KEYBOARD_HEIGHT = 352;
        PORTRAIT_KEYBOARD_HEIGHT = 264;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
