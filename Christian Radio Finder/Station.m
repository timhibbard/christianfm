//
//  Station.m
//  Christian Radio Finder
//
//  Created by Tim Hibbard on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Station.h"


@implementation Station

@dynamic callSign;
@dynamic frequency;
@dynamic city;
@dynamic lat;
@dynamic lng;
@dynamic ownedBy;
@dynamic distance;
@dynamic streamUrl;
@dynamic website;
@dynamic twitter;
@dynamic facebook;
@dynamic range;
@dynamic signalStrength;
@dynamic backgroundStreamUrl;
@dynamic databaseId;

@end
