//
//  StationMapViewController.h
//  Christian FM
//
//  Created by Tim Hibbard on 8/30/12.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Station.h"
#import "StationLookupManager.h"
#import "StationDetailTableViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface StationMapViewController : UIViewController<MKMapViewDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate>

@property (nonatomic, retain) IBOutlet MKMapView *map;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSMutableArray *stationArray;
@property (nonatomic) double smallestLat;
@property (nonatomic) double smallestLng;
@property (nonatomic) double biggestLat;
@property (nonatomic) double biggestLng;
@property (nonatomic) BOOL isSearching;
@property (nonatomic) CLLocationCoordinate2D searchLocation;
@property (nonatomic,retain) IBOutlet UILabel *infoLabel;
@property (nonatomic) NSString *helpText;
@property (nonatomic) NSInteger locationHitCount;
@property (nonatomic) MKCircle *selectedCircle;

- (IBAction)longPress:(UILongPressGestureRecognizer *)gestureRecognizer;
- (IBAction)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
- (IBAction)searchBarCancelButtonClicked:(UISearchBar *)searchBar;
- (IBAction)shareButtonClicked:(id)sender;

- (void) addOverlayFromStationArray;

@end
