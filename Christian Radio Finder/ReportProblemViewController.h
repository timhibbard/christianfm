//
//  ReportProblemViewController.h
//  Christian FM
//
//  Created by Tim Hibbard on 11/15/12.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Station.h"

@class Station;

@interface ReportProblemViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource>


@property (nonatomic, strong) Station *station;
@property (nonatomic, retain) IBOutlet UIPickerView *picker;
@property (nonatomic, retain) IBOutlet UITableViewCell *issueCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *noteCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *submitCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *cancelCell;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSInteger locationHitCount;


@end
