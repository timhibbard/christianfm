//
//  StationLookupManager.m
//  Christian FM
//
//  Created by Tim Hibbard on 8/19/12.
//
//

#import "StationLookupManager.h"
#import "Station.h"
#import "AppDelegate.h"


static void (^failure)(NSString *) = nil;
static void (^success)(NSMutableArray *) = nil;




@implementation StationLookupManager

@synthesize stationArray;
@synthesize xmlStation;
@synthesize currentElementValue;

- (void) getData:(NSString *)stationName :(void (^)(NSString *))executeOnFailure :(void (^)(NSMutableArray *))executeOnSuccess{
    failure = executeOnFailure;
    success = executeOnSuccess;
    
    //NSString *sq = [NSString stringWithFormat:@"ownedby==%@ or callsign==%@",stationName,stationName];
    NSString *sq = [NSString stringWithFormat:@"ownedby=\"%@\" or callsign=\"%@\" or callsign=\" %@ \"",[stationName stringByReplacingOccurrencesOfString:@" " withString:@"+"],stationName, stationName];
    NSString * encodedString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                     NULL,
                                                                                                     (__bridge CFStringRef)sq,
                                                                                                     NULL,
                                                                                                     (CFStringRef)@"!*'();:@&=$,/?%#[]",
                                                                                                     kCFStringEncodingUTF8 );
    NSString *urlString = [NSString stringWithFormat:@"https://spreadsheets.google.com/feeds/list/0Ahgt-_OuYTzMdHNwVmd3MnRHTlB5MUNIMFFvNFZHa2c/od6/public/values?sq=%@",encodedString];
    NSLog(@"url=%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [xmlParser setDelegate:(id)self];
    
    if ([xmlParser parse]) {
        NSLog(@"No errors in xmlParser parse");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        success(stationArray);
    }
    else{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"error error errror");
        failure(@"Could not parse XML");
    }
    
}

- (void) getData:(double)gpsLat :(double)gpsLng : (NSInteger ) range :(void (^)(NSString *))executeOnFailure :(void (^)(NSMutableArray *))executeOnSuccess{
    
    failure = executeOnFailure;
    success = executeOnSuccess;
    double delta = 1.2;
    if (range > 0) {
        delta = delta + ((int)range / (double)60);
    }
    NSLog(@"delta:%lf",delta);
    NSString *sq = [NSString stringWithFormat:@"lat < %lf and lat > %lf and long > %lf and long < %lf",gpsLat + delta, gpsLat - delta, gpsLng - delta, gpsLng + delta];
    
    NSString * encodedString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                   NULL,
                                                                                   (__bridge CFStringRef)sq,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                   kCFStringEncodingUTF8 );
    
    //NSString *urlString = [NSString stringWithFormat:@"https://spreadsheets.google.com/feeds/list/0Ahgt-_OuYTzMdHNwVmd3MnRHTlB5MUNIMFFvNFZHa2c/od6/public/basic?sq=%@",encodedString];
    
    NSString *urlString = [NSString stringWithFormat:@"https://spreadsheets.google.com/feeds/list/0Ahgt-_OuYTzMdHNwVmd3MnRHTlB5MUNIMFFvNFZHa2c/od6/public/values?sq=%@",encodedString];
    NSLog(@"url=%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [xmlParser setDelegate:(id)self];
    
    if ([xmlParser parse]) {
        NSLog(@"No errors in xmlParser parse");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        success(stationArray);
    }
    else{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"error error errror");
        failure(@"Could not parse XML");
    }
}

- (void)parser: (NSXMLParser *) parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"feed"]) {
        stationArray = [[NSMutableArray alloc]init];
        return;
    }
    
    if ([elementName isEqualToString:@"entry"]) {
        NSManagedObjectContext *moc = [(AppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
        NSEntityDescription *stationDescription = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:moc];
        xmlStation = [[Station alloc] initWithEntity:stationDescription insertIntoManagedObjectContext:moc];
        [xmlStation setRange:[[NSDecimalNumber alloc] initWithDouble:30.0]];
        return;
    }
    
    currentElementValue = nil;
    
    //NSLog(@"processing element: %@",elementName);
}

- (void)parser: (NSXMLParser *) parser foundCharacters:(NSString *)string{
    if (!currentElementValue) {
        currentElementValue = [[NSMutableString alloc] initWithString:string];
        return;
    }
    [currentElementValue appendString:string];
    //NSLog(@"Processing value: %@",currentElementValue);
    
}

- (void)parser: (NSXMLParser *) parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if ([elementName isEqualToString:@"feed"]) {
        return;
    }
    
    if ([elementName isEqualToString:@"entry"]) {
        [stationArray addObject:xmlStation];
        xmlStation = nil;
        return;
    }
    
    //NSLog(@"item:%@,length:%i",currentElementValue,currentElementValue.length);
    NSString *value = [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //NSLog(@"item:%@,length:%i",value,value.length);
    
    if ([elementName isEqualToString:@"gsx:callsign"]) {
        [xmlStation setCallSign:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:freq"]) {
        [xmlStation setFrequency:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:city"]) {
        [xmlStation setCity:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:lat"]) {
        [xmlStation setLat:[NSDecimalNumber decimalNumberWithString:value]];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:long"]) {
        [xmlStation setLng:[NSDecimalNumber decimalNumberWithString:value]];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:ownedby"]) {
        [xmlStation setOwnedBy:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:streamurl"]) {
        [xmlStation setStreamUrl:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:backgroundstreamingurl"]) {
        [xmlStation setBackgroundStreamUrl:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:id"]) {
        [xmlStation setDatabaseId:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:website"]) {
        [xmlStation setWebsite:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:twitter"]) {
        [xmlStation setTwitter:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:fb"]) {
        [xmlStation setFacebook:value];
        return;
    }
    
    if ([elementName isEqualToString:@"gsx:range"]) {
        if (value == nil) {
            return;
        }
        [xmlStation setRange:[NSDecimalNumber decimalNumberWithString:value]];
        return;
    }
}



@end
