//
//  Station.h
//  Christian Radio Finder
//
//  Created by Tim Hibbard on 7/14/12.
//  Copyright (c) 2012 EnGraph Software All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Station : NSManagedObject

@property (nonatomic) NSString * callSign;
@property (nonatomic) NSString * frequency;
@property (nonatomic) NSString * city;
@property (nonatomic) NSDecimalNumber * lat;
@property (nonatomic) NSDecimalNumber * lng;
@property (nonatomic) NSString * ownedBy;
@property (nonatomic) NSString * distance;
@property (nonatomic) NSString *streamUrl;
@property (nonatomic) NSString *backgroundStreamUrl;
@property (nonatomic) NSString *website;
@property (nonatomic) NSString *twitter;
@property (nonatomic) NSString *facebook;
@property (nonatomic) NSDecimalNumber *range;
@property (nonatomic) NSDecimalNumber *signalStrength;
@property (nonatomic) NSString *databaseId;
@end
