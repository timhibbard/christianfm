//
//  StationDetailTableViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 8/24/12.
//
//

#import "StationDetailTableViewController.h"
#import "WebViewController.h"
#import "MapDetailViewController.h"

@interface StationDetailTableViewController ()

@end

@implementation StationDetailTableViewController{
    AudioStreamer *streamer;
}

@synthesize station;
@synthesize freqCell;
@synthesize facebookCell;
@synthesize twitterCell;
@synthesize ownedByCell;
@synthesize streamingCell;
@synthesize geoCell;
@synthesize reminderCell;
@synthesize favoriteCell;
@synthesize reportProblemCell;
@synthesize eventId;
@synthesize eventStore;
@synthesize providedStoreAccess;

- (void) shareButtonClicked:(id)sender{
    UIActionSheet *sheet =
    [[UIActionSheet alloc] initWithTitle:
     NSLocalizedString(@"Share station information",
                       @"Title for sheet displayed with options for sharing station information with other applications")
                                delegate:(id)self
                       cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                  destructiveButtonTitle:nil
                       otherButtonTitles:NSLocalizedString(@"Email", @"Email"),NSLocalizedString(@"SMS", @"SMS"),
     nil];
    [sheet showInView:self.view];
}

// Called when the user selects an option in the sheet. The sheet will automatically be dismissed.
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc]
                                                           init];
                controller.mailComposeDelegate = (id)self;
                [controller setSubject:@"Radio Station Information"];
                
                NSString *emailBody = [NSString stringWithFormat:@"%@ (%@) located in %@\n%@ away\n",station.frequency,station.ownedBy,station.city,station.distance];
  
                
                [controller setMessageBody:emailBody isHTML:NO];
                [self presentModalViewController:controller animated:YES];
                
            }
        } break;
        case 1: {
            if ([MFMessageComposeViewController canSendText])
            {
                MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc]
                                                                 init];
                smsController.messageComposeDelegate = (id)self;
                
                NSString *textBody = [NSString stringWithFormat:@"%@ located in %@,%@ away",station.frequency,station.city,station.distance];
                [smsController setBody:textBody];
                [self presentModalViewController:smsController animated:YES];
                
            }
        } break;

        default:
            break;
    }
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [self dismissModalViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    [self setTitle:station.frequency];
   
    [freqCell.textLabel setText:[NSString stringWithFormat:@"%@ (%@)",station.frequency,station.callSign]];
    [freqCell.textLabel setTextColor:[UIColor grayColor]];
    [freqCell setUserInteractionEnabled:NO];
    [ownedByCell.textLabel setText:station.ownedBy];
    if ([station.website length]>0) {
        [ownedByCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [ownedByCell.textLabel setText:[NSString stringWithFormat:@"%@ website", station.ownedBy]];
    }
    [twitterCell.textLabel setText:station.twitter];
    [geoCell.textLabel setText:[NSString stringWithFormat:@"%@ %@ away",station.city,station.distance]];
    
    if (station.ownedBy.length == 0) {
        [ownedByCell.textLabel setText:@"No info"];
        [ownedByCell setUserInteractionEnabled:NO];
        [ownedByCell.textLabel setTextColor:[UIColor grayColor]];
    }
    
    if (station.streamUrl.length == 0) {
        [streamingCell.textLabel setText:@"No online stream"];
        [streamingCell.textLabel setTextColor:[UIColor grayColor]];
        [streamingCell setUserInteractionEnabled:NO];
        [streamingCell setAccessoryType:UITableViewCellAccessoryNone];
        [streamingCell.imageView setHidden:YES];
    }
    else{
        if (station.backgroundStreamUrl.length > 0) {
            [streamingCell.textLabel setText:@"Listen online (background)"];
            [streamingCell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
        
    
    
    if (station.facebook.length == 0) {
        [facebookCell.textLabel setText:@"No Facebook info"];
        [facebookCell setUserInteractionEnabled:NO];
        [facebookCell.textLabel setTextColor:[UIColor grayColor]];
        [facebookCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    if (station.twitter.length == 0) {
        [twitterCell.textLabel setText:@"No Twitter info"];
        [twitterCell setUserInteractionEnabled:NO];
        [twitterCell.textLabel setTextColor:[UIColor grayColor]];
        [twitterCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    [self setReminderCellDisplay];
    [self setFavoriteCellDisplay];
    

}

- (void) setFavoriteCellDisplay{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *favorites = [defaults objectForKey:@"favorites"];
    
    if (favorites.count == 0) {
        return;
    }
    
    for (NSString *key in favorites) {
        if ([[NSString stringWithFormat:@"%@%@",station.lat,station.lng] isEqualToString:key]) {
            [favoriteCell.textLabel setText:@"Added as favorite"];
            [favoriteCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            return;
        }
    }
    
}

- (void) setReminderCellDisplay{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *reminders = [defaults objectForKey:@"reminders"];
    
    if (reminders.count == 0) {
        return;
    }
    
    EKEventStore *store = [self getEventStore];
    if (!store) {
        return;
    }
    if (!providedStoreAccess) {
        return;
    }
    NSMutableArray *remindersToRemove = [[NSMutableArray alloc] init];
    for (NSString *key in reminders) {
        //making sure that no events have been removed outside of this application
        //if so, we are going to remove it from the collection and not mark the reminder cell
        BOOL skip = NO;
        NSString *eventIdFromReminders = [reminders valueForKey:key];
        EKReminder *reminder = (EKReminder *)[store calendarItemWithIdentifier:eventIdFromReminders];
        if (!reminder || reminder.completed) {
            [remindersToRemove addObject:eventIdFromReminders];
            skip = YES;
            NSLog(@"Need to remove:%@",eventIdFromReminders);
        }
        if ([[NSString stringWithFormat:@"%@%@",station.lat,station.lng] isEqualToString:key] && !skip) {
            [reminderCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [self setEventId:eventIdFromReminders];
        }
    }
    //nothing to remove...carry on
    if (remindersToRemove.count == 0) {
        return;
    }
    
    NSMutableArray *latlng = [NSMutableArray arrayWithArray:reminders.allKeys];
    NSMutableArray *itemIds = [NSMutableArray arrayWithArray:reminders.allValues];
    
    for (NSString *eventIdToRemove in remindersToRemove) {
        NSUInteger index = [itemIds indexOfObject:eventIdToRemove];
        [latlng removeObjectAtIndex:index];
        [itemIds removeObjectAtIndex:index];
        NSLog(@"Removed:%@",eventIdToRemove);
    }
    
    NSDictionary *newReminders = [NSDictionary dictionaryWithObjects:itemIds forKeys:latlng];
    [defaults setObject:newReminders forKey:@"reminders"];
    [defaults synchronize];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"OpenWebsiteSegue"]) {
        WebViewController *webController = [segue destinationViewController];
        [webController setWebTitle:station.ownedBy];
        [webController setUrlString:station.website];
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"StreamAudioSegue"]) {
        WebViewController *webController = [segue destinationViewController];
        [webController setWebTitle:station.ownedBy];
        [webController setUrlString:station.streamUrl];
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"OpenFacebookSegue"]) {
        WebViewController *webController = [segue destinationViewController];
        [webController setWebTitle:station.ownedBy];
        [webController setUrlString:station.facebook];
        
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"OpenTwitterSegue"]) {
        WebViewController *webController = [segue destinationViewController];
        [webController setWebTitle:station.ownedBy];
        NSString *twitterHandle = [station.twitter stringByReplacingOccurrencesOfString:@"@" withString:@""];
        [webController setUrlString:[NSString stringWithFormat:@"http://twitter.com/%@",twitterHandle]];
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"ViewMapSegue"]){
        MapDetailViewController *mapController = [segue destinationViewController];
        [mapController setStation:station];
        return;
    }
    
    if ([[segue identifier] isEqualToString:@"ReportProblemSegue"]){
        ReportProblemViewController *reportProblemController = [segue destinationViewController];
        [reportProblemController setStation:station];
        return;
    }
    

    
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    providedStoreAccess = YES;

    
    //[defaults setObject:helpText forKey:@"helpText"];
    //[defaults synchronize];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - conditional app launchers
-(void) launchFacebook{
    
    //can't get the fb:// scheme to work
    [self performSegueWithIdentifier:@"OpenFacebookSegue" sender:self];
    return;
    
    NSString *fbName = [station.facebook stringByReplacingOccurrencesOfString:@"https://www.facebook.com/" withString:@""];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"fb://%@",fbName]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return;
    }
    
    [self performSegueWithIdentifier:@"OpenFacebookSegue" sender:self];
    
}

- (void) launchTwitter{
    NSString *twitterHandle = [station.twitter stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tweetbot:///user_profile/%@",twitterHandle]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return;
    }
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"echofon:///user_timeline?%@",twitterHandle]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return;
    }
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://user?screen_name=%@",twitterHandle]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return;
    }
    
    [self performSegueWithIdentifier:@"OpenTwitterSegue" sender:self];
}

#pragma mark - reminders

- (EKEventStore*) getEventStore{
    if (eventStore) {
        return eventStore;
    }
    
    EKEventStore *store = [[EKEventStore alloc] init];
    __block BOOL providedAccess = YES;
    BOOL atLeastIOS6 = [[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0;
    if (!atLeastIOS6) {
        providedStoreAccess = NO;
        return nil;
    }
    [store requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
        providedAccess = granted;
    }];
    [self setProvidedStoreAccess:providedAccess];
    [self setEventStore:store];
    return store;
}

- (BOOL) addReminder{
    EKEventStore *store = [self getEventStore];
    if (!store) {
        return NO;
    }
    if (!providedStoreAccess) {
        return NO;
    }
    EKReminder *reminder = [EKReminder reminderWithEventStore:store];
    [reminder setTitle:[NSString stringWithFormat:@"Listen to %@ (%@)",station.ownedBy,station.frequency]];
    [reminder setNotes:[NSString stringWithFormat:@"%@ %i mile range",station.city, (int)[station.range doubleValue]]];
    EKAlarm *enterAlarm = [[EKAlarm alloc] init];
    [enterAlarm setProximity:EKAlarmProximityEnter];
    EKStructuredLocation *enterLocation = [EKStructuredLocation locationWithTitle:[NSString stringWithFormat:@"%@ (%@)",station.ownedBy,station.frequency]];
    CLLocationDegrees lat = [station.lat doubleValue];
    CLLocationDegrees lng = [station.lng doubleValue];
    CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    [enterLocation setGeoLocation:radioLocation];
    [enterLocation setRadius:[station.range doubleValue] * 1609];
    [enterAlarm setStructuredLocation:enterLocation];
    [reminder addAlarm:enterAlarm];
    [reminder setCalendar:[store defaultCalendarForNewReminders]];

    NSError *err;
    [store saveReminder:reminder commit:YES error:&err];
    [self setEventId:reminder.calendarItemIdentifier];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *reminders = [defaults objectForKey:@"reminders"];
    NSMutableArray *latlng = [NSMutableArray arrayWithArray:reminders.allKeys];
    NSMutableArray *itemIds = [NSMutableArray arrayWithArray:reminders.allValues];
    
    [latlng addObject:[NSString stringWithFormat:@"%@%@",station.lat,station.lng]];
    [itemIds addObject:eventId];
    
    NSDictionary *newReminders = [NSDictionary dictionaryWithObjects:itemIds forKeys:latlng];
    [defaults setObject:newReminders forKey:@"reminders"];
    [defaults synchronize];
    
    return YES;
}



- (BOOL) removeReminder {
    if (eventId.length == 0) {
        return NO;
    }
    EKEventStore *store = [self getEventStore];
    if (!store) {
        return NO;
    }
    if (!providedStoreAccess) {
        return NO;
    }
    
    EKReminder *reminder = (EKReminder *)[store calendarItemWithIdentifier:eventId];

    NSError *err;
    [store removeReminder:reminder commit:YES error:&err];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *reminders = [defaults objectForKey:@"reminders"];
    NSMutableArray *latlng = [NSMutableArray arrayWithArray:reminders.allKeys];
    NSMutableArray *itemIds = [NSMutableArray arrayWithArray:reminders.allValues];
    
    [latlng removeObject:[NSString stringWithFormat:@"%@%@",station.lat,station.lng]];
    [itemIds removeObject:eventId];
    
    NSDictionary *newReminders = [NSDictionary dictionaryWithObjects:itemIds forKeys:latlng];
    [defaults setObject:newReminders forKey:@"reminders"];
    [defaults synchronize];
    
    [self setEventId:@""];
    return YES;
    
}

- (BOOL) removeFavorite{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *favorites = [defaults objectForKey:@"favorites"];
    NSMutableArray *latlng = [NSMutableArray arrayWithArray:favorites.allKeys];
    NSMutableArray *itemIds = [NSMutableArray arrayWithArray:favorites.allValues];
    
    [latlng removeObject:[NSString stringWithFormat:@"%@%@",station.lat, station.lng]];
    [itemIds removeObject:station.databaseId];
    
    NSDictionary *newFavorites = [NSDictionary dictionaryWithObjects:itemIds forKeys:latlng];
    [defaults setObject:newFavorites forKey:@"favorites"];
    [defaults synchronize];
    
    return YES;
}

- (BOOL) addFavorite{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *favorites = [defaults objectForKey:@"favorites"];
    NSMutableArray *latlng = [NSMutableArray arrayWithArray:favorites.allKeys];
    NSMutableArray *itemIds = [NSMutableArray arrayWithArray:favorites.allValues];
    
    [latlng addObject:[NSString stringWithFormat:@"%@%@",station.lat, station.lng]];
    [itemIds addObject:station.databaseId];
    
    NSDictionary *newFavorites = [NSDictionary dictionaryWithObjects:itemIds forKeys:latlng];
    [defaults setObject:newFavorites forKey:@"favorites"];
    [defaults synchronize];
    
    return  YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;

    NSInteger sections = 2;
    if (station.ownedBy.length > 0 ||
        station.streamUrl.length > 0 ||
        station.facebook.length > 0 ||
        station.twitter.length > 0) {
        sections = sections + 1;
    }
    // Return the number of sections.
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 3;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return 2;
            break;
        case 4:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        [self launchFacebook];
    }
    if (indexPath.section == 1 && indexPath.row == 2) {
        [self launchTwitter];
    }
    
    if (indexPath.section == 3 && indexPath.row == 0) {
        if (reminderCell.accessoryType == UITableViewCellAccessoryCheckmark) {
            if ([self removeReminder]) {
                [reminderCell setAccessoryType:UITableViewCellAccessoryNone];
            }
        }
        else{
            if ([self addReminder]) {
                [reminderCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
        
        }
    }
    
    if (indexPath.section == 3 && indexPath.row == 1) {
        //favorite
        if (favoriteCell.accessoryType == UITableViewCellAccessoryCheckmark) {
            if ([self removeFavorite]){
                [favoriteCell.textLabel setText:@"Add as favorite"];
                [favoriteCell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
        else {
            if ([self addFavorite]) {
                [favoriteCell.textLabel setText:@"Added as favorite"];
                [favoriteCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
        
        }
    }
    
    if (indexPath.section == 0 && indexPath.row == 1) {
        //play the background stream if we have it
        if (station.backgroundStreamUrl.length > 0) {
            //[(AppDelegate *)[[UIApplication sharedApplication] delegate] playStream:station.backgroundStreamUrl];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if ([delegate.currentlyPlayingUrl isEqualToString:station.backgroundStreamUrl]) {
                if (delegate.isLoadedAndPlaying){
                    [delegate pauseStream];
                    return;
                }
                if (delegate.isLoadedAndPaused) {
                    [delegate playCurrentStream];
                    return;
                }
            }
            [delegate playStream:station.backgroundStreamUrl];
            return;
        }
        
        //otherwise just shell out to safari
        //make sure we are not currently streaming first
        if (station.streamUrl.length > 0) {
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] destroyStream];
            [self performSegueWithIdentifier:@"StreamAudioSegue" sender:self];
        }
        
    }
    
    
    
    if (indexPath.section == 4 && indexPath.row == 0) {
        //report problem
        [self performSegueWithIdentifier:@"ReportProblemSegue" sender:self];
    }
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        return freqCell;
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        return streamingCell;
    }
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        return ownedByCell;
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        return facebookCell;
    }
    if (indexPath.section == 1 && indexPath.row == 2) {
        return twitterCell;
    }
    
    
    if (indexPath.section == 2 && indexPath.row == 0) {
        return geoCell;
    }
    
    
    if (indexPath.section == 3 && indexPath.row == 0) {
        return reminderCell;
    }
    if (indexPath.section == 3 && indexPath.row == 1) {
        return favoriteCell;
    }
    
    if (indexPath.section == 4 && indexPath.row == 0) {
        return reportProblemCell;
    }
    
    return nil;
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



@end
