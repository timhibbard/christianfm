//
//  StationCellCell.h
//  Christian FM
//
//  Created by Tim Hibbard on 7/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationCell : UITableViewCell{
    
}


@property (nonatomic, retain) IBOutlet UILabel *frequencyLabel;
@property (nonatomic, retain) IBOutlet UILabel *cityLabel;
@property (nonatomic, retain) IBOutlet UILabel *callSignLabel;
@property (nonatomic, retain) IBOutlet UILabel *distanceLabel;


@end
