//
//  StationLookupManager.h
//  Christian FM
//
//  Created by Tim Hibbard on 8/19/12.
//
//

#import <Foundation/Foundation.h>
#import "Station.h"

@interface StationLookupManager : NSObject{
    NSMutableData *webData;
    NSURLConnection *conn;
}

@property (nonatomic) NSMutableArray *stationArray;
@property (nonatomic) NSMutableString *currentElementValue;
@property (nonatomic, strong) Station *xmlStation;

- (void) getData: (double ) gpsLat : (double ) gpsLng : (NSInteger ) range : (void (^) (NSString *errorMessage)) executeOnFailure : (void (^)(NSMutableArray *xmlStationArray)) executeOnSuccess;

- (void) getData: (NSString *) stationName : (void (^) (NSString *errorMessage)) executeOnFailure : (void (^)(NSMutableArray *xmlStationArray)) executeOnSuccess;

@end
