//
//  StationTableViewController_iPhone.h
//  Christian Radio Finder
//
//  Created by Tim Hibbard on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "station.h"
#import <CoreLocation/CoreLocation.h>
#import "PullTableView.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Station.h"
#import <CoreData/CoreData.h>
#import "PullTableView.h"
#import "StationCell.h"
#import "MapDetailViewController.h"
#import "StationLookupManager.h"
#import "StationDetailTableViewController.h"
#import "StationMapViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface StationTableViewController : UITableViewController<UITableViewDataSource, PullTableViewDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate>{
    NSManagedObjectContext *managedObjectContext;
    NSMutableArray *stationArray;
    IBOutlet CLLocationManager *locationManager;
    NSInteger selectedRowIndex;
    BOOL selectionHasBeenMade;
}

typedef enum {
    Owner,
    City,
    Strength,
    Distance,
    Range
} displayDetail;

@property (nonatomic,retain) UILabel *loadMore;
@property (nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) NSMutableArray *stationArray;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic, retain) IBOutlet PullTableView *pullTableView;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic) NSInteger selectedRowIndex;
@property (nonatomic) NSInteger rangeDelta;
@property (nonatomic) BOOL selectionHasBeenMade;
@property (nonatomic) displayDetail displayInDetails;
@property (nonatomic) NSString *helpText;




- (void) fetchByLocation;
- (IBAction)longPress:(UILongPressGestureRecognizer *)gestureRecognizer;
- (IBAction)swipeRight:(UISwipeGestureRecognizer *) swipeRightRecognizer;
- (IBAction)swipeLeft:(UISwipeGestureRecognizer *) swipeLeftRecognizer;
- (IBAction)swipeUp:(UISwipeGestureRecognizer *) swipeUpRecognizer;
- (IBAction)swipeDown:(UISwipeGestureRecognizer *) swipeDownRecognizer;

-(IBAction)shareToolbarClick:(id)sender;
-(IBAction)refreshToolbarClick:(id)sender;
-(IBAction)playToolbarClick:(id)sender;
-(IBAction)pauseToolbarClick:(id)sender;
@end
