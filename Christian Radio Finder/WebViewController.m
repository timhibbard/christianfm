//
//  WebViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 8/25/12.
//
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

@synthesize urlString;
@synthesize webView;
@synthesize webTitle;

- (void) shareButtonClicked:(id)sender{
    UIActionSheet *sheet =
    [[UIActionSheet alloc] initWithTitle:
     NSLocalizedString(@"Action",
                       @"Title for sheet displayed with options for sharing station information with other applications")
                                delegate:(id)self
                       cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                  destructiveButtonTitle:nil
                       otherButtonTitles:NSLocalizedString(@"Open in Safari", @"Open In Safari"),
     nil];
    [sheet showInView:self.view];
}

// Called when the user selects an option in the sheet. The sheet will automatically be dismissed.
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        } break;

        default:
            break;
    }
    
}

- (void) viewWillAppear:(BOOL)animated{
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:webTitle];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
	// Do any additional setup after loading the view.
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
