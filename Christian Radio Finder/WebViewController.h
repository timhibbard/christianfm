//
//  WebViewController.h
//  Christian FM
//
//  Created by Tim Hibbard on 8/25/12.
//
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString *webTitle;

- (IBAction)shareButtonClicked:(id)sender;

@end
