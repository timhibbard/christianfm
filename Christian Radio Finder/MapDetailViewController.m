//
//  StationDetailViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 8/4/12.
//
//

#import "MapDetailViewController.h"



@interface MapDetailViewController ()

@end

@implementation MapDetailViewController

@synthesize station;
@synthesize distanceLabel;
@synthesize map;
@synthesize locationManager;
@synthesize displayView;
@synthesize gotFirstHit;
@synthesize gotInitalViewport;

-(void)shareButtonClicked:(id)sender{
    UIActionSheet *sheet =
    [[UIActionSheet alloc] initWithTitle:
     NSLocalizedString(@"Share station information",
                       @"Title for sheet displayed with options for sharing station information with other applications")
                                delegate:(id)self
                       cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                  destructiveButtonTitle:nil
                       otherButtonTitles:
     NSLocalizedString(@"Show Location in Maps", @"Show Location in Maps"),
     nil];
    [sheet showInView:self.view];
    //[sheet release];
}

// Called when the user selects an option in the sheet. The sheet will automatically be dismissed.
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {

    switch (buttonIndex) {
        case 0: {
            NSString *mapsQuery =
            [NSString stringWithFormat:@"http://maps.google.com/maps?z=6&t=h&ll=%lf,%lf",
             station.lat.doubleValue, station.lng.doubleValue];
            BOOL atLeastIOS6 = [[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0;
            if (atLeastIOS6) {
                mapsQuery = [NSString stringWithFormat:@"http://maps.apple.com/maps?q=%lf,%lf&ll=%lf,%lf&z=8",
                 station.lat.doubleValue, station.lng.doubleValue,(station.lat.doubleValue + map.userLocation.location.coordinate.latitude)/2,(station.lng.doubleValue + map.userLocation.location.coordinate.longitude)/2];
            }
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mapsQuery]];
        } break;
        default:
            break;
    }

}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setTitle:station.frequency];

    [distanceLabel setText:[NSString stringWithFormat:@"%@ away",station.distance]];
    MKPointAnnotation *stationLocation = [[MKPointAnnotation alloc] init];
    CLLocationDegrees lat = [station.lat doubleValue];
    CLLocationDegrees lng = [station.lng doubleValue];
    stationLocation.coordinate = CLLocationCoordinate2DMake(lat, lng);
    [stationLocation setTitle:station.frequency];
    [stationLocation setSubtitle:station.ownedBy];
    [map addAnnotation:stationLocation];
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:stationLocation.coordinate radius:[station.range doubleValue] * 1609];
    [map addOverlay:circle];
    

    locationManager = [[CLLocationManager alloc]init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.delegate = (id)self;
    [locationManager startUpdatingLocation];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [locationManager stopUpdatingLocation];
    [locationManager setDelegate:nil];
}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay{
    MKCircleView *circleView = [[MKCircleView alloc] initWithCircle:(MKCircle *)overlay];
    //circleView.strokeColor=[UIColor blueColor];
    circleView.fillColor=[[UIColor blueColor] colorWithAlphaComponent:0.1];
    return circleView;
}

- (void) locationManager: (CLLocationManager *) manager
     didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    if (!gotFirstHit) {
        gotFirstHit = YES;
        return;
    }
    
    //[locationManager stopUpdatingLocation];
    //locationManager.delegate = nil;
    
    NSLog(@"hit lat:%lf lng:%lf",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    
    CLLocationDegrees lat = [station.lat doubleValue];
    CLLocationDegrees lng = [station.lng doubleValue];
    CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    CLLocationDistance distance = [newLocation distanceFromLocation:radioLocation] * 0.000621371192;
    
//    if (!gotInitalViewport) {
//        gotInitalViewport = YES;
//        CLLocationDegrees centerLat = ([station.lat doubleValue] + newLocation.coordinate.latitude) / 2;
//        CLLocationDegrees centerLng = ([station.lng doubleValue] + newLocation.coordinate.longitude) / 2;
//        
//        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(centerLat, centerLng);
//        
//        MKCoordinateSpan span = MKCoordinateSpanMake(distance / 55, distance / 55);
//        MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
//        [map setRegion:region animated:YES];
//        [map regionThatFits:region];
//    }
    
    if (!gotInitalViewport) {
        gotInitalViewport = YES;
        MKCoordinateRegion region = MKCoordinateRegionMake(radioLocation.coordinate, MKCoordinateSpanMake([station.range doubleValue]/30, [station.range doubleValue]/30));
        [map setRegion:region animated:YES];
        [map regionThatFits:region];
    }
    
    
    NSString *labelText = [NSString stringWithFormat:@"%d miles away",(int) ceil(distance)];
    if ([[distanceLabel text] isEqualToString:labelText]) {
        return;
    }
    [distanceLabel setText:labelText];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    displayView.layer.cornerRadius = 8;
    displayView.layer.masksToBounds = YES;
    
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
