//
//  AboutViewController.m
//  Christian FM
//
//  Created by Tim Hibbard on 10/10/12.
//
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3) {
        switch (indexPath.row) {
            case 0:
                [Appirater rateApp];
                break;
                
            case 1:{
                if (![self launchTwitter:@"CRLApp"]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/CRLApp"]];
                }
            }break;
            case 2:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/pages/Christian-Radio-Locator/372509509486534"]];
                break;
            case 3:{
                if (![self launchTwitter:@"timhibbard"]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/timhibbard"]];
                }
            case 4:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://timhibbard.com"]];
                break;
            case 5:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://engraph.com"]];
                break;
            }break;
            default:
                break;
        }
    }
    
    if (indexPath.section == 4) {
        switch (indexPath.row) {
            case 0:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://chelseablogs.com"]];
                break;
                
            case 1:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://github.com/arashpayan/appirater/"]];
                break;
            case 2:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://radio-locator.com"]];
                break;
            case 3:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://github.com/alexcrichton/AudioStreamer"]];
                break;
            default:
                break;
        }
    }
    
    if (indexPath.section == 5) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"Press and hold to search" forKey:@"mapHelpText"];
        [defaults setObject:@"Swipe left to see more info" forKey:@"helpText"];
        [defaults synchronize];
        
    }
    
    if (indexPath.section == 6) {
        [self dismissModalViewControllerAnimated:YES];
    }
}

#pragma mark - conditional app launchers

- (BOOL) launchTwitter: (NSString *) twitterHandle{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tweetbot:///user_profile/%@",twitterHandle]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return YES;
    }
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"echofon:///user_timeline?%@",twitterHandle]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return YES;
    }
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://user?screen_name=%@",twitterHandle]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        return YES;
    }
    
    return NO;
} 

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



@end
